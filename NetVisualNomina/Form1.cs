﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NetVisualNomina
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void nuevaVentanaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmNew newWindows = new FrmNew();
            newWindows.MdiParent = this;
            newWindows.Show();
        }
    }
}
