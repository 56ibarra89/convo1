﻿using Nomina.Business.Entities;

namespace Nomina.Business.Dao
{
    public interface IDaoEmpleado : IDao<Empleado>
    {
        Empleado findById(int id);
        Empleado findByCedula(string cedula);
    }
}
