﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmMain : Form
    {
        private int RowIndex;
        private DaoEmpleadoImplements daoEmpleadoImplements;
        private List<Empleado> empleados;
        private BindingSource bsEmpleados;
        public FrmMain()
        {
            daoEmpleadoImplements = new DaoEmpleadoImplements();
            bsEmpleados = new BindingSource();
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            empleados = daoEmpleadoImplements.All();

            empleados.ForEach(ep => {
                dsNomina.Tables["Empleado"].Rows.Add(ep.EmpleadoAsArray());
            });

            bsEmpleados.DataSource = dsNomina;
            bsEmpleados.DataMember = dsNomina.Tables["Empleado"].TableName;

            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmEmpleado frmEmpleado = new FrmEmpleado();
            frmEmpleado.DSNomina = dsNomina;
            frmEmpleado.ShowDialog(this);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollection = dgvEmpleados.SelectedRows;

            if (rowCollection.Count == 0)
            {
                MessageBox.Show(this, "ERROR, debe seleccionar una fila de la tabla para poder editar", "Mensaje de ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            if (rowCollection.Count > 0)
            {

                int index = dgvEmpleados.SelectedRows[0].Index;


                int id = Convert.ToInt32(dgvEmpleados.Rows[index].Cells[0].Value.ToString());
                string cedula = dgvEmpleados.Rows[index].Cells[1].Value.ToString();
                string nombres = dgvEmpleados.Rows[index].Cells[2].Value.ToString();
                string apellidos = dgvEmpleados.Rows[index].Cells[3].Value.ToString();
                string direccion = dgvEmpleados.Rows[index].Cells[4].Value.ToString();
                string telefono = dgvEmpleados.Rows[index].Cells[5].Value.ToString();
                DateTime fecha = Convert.ToDateTime(dgvEmpleados.Rows[index].Cells[6].Value.ToString());
                float salario = Convert.ToSingle(dgvEmpleados.Rows[index].Cells[7].Value);

                Empleado empleado = new Empleado()
                {
                    Id = id,
                    Nombres = nombres,
                    Apellidos = apellidos,
                    Cedula = cedula,
                    Telefono = telefono,
                    Direccion = direccion,
                    Salario = (decimal)salario,
                    FechaContratacion = fecha
                };

                DialogResult result = MessageBox.Show(this, "Realmente desea eliminar ese registro?", "Mensaje del Sistema", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    MessageBox.Show(this, "Registro eliminado satisfactoriamente!", "Mensaje del Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgvEmpleados.Rows.RemoveAt(index);
                    daoEmpleadoImplements.Delete(empleado);

                }
            }
    }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dgvEmpleados.Rows[RowIndex].Cells[0].Value == null)
            {
                MessageBox.Show("Seleccione el campo a modificar.",
                "Campo no seleccionado.");
                return;
            }

            FrmActualizar frmActualizar = new FrmActualizar();
            frmActualizar.Row = RowIndex;
            frmActualizar.ShowDialog(this);
            //load();
        }
    }
}
