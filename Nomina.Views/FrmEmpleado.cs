﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using Nomina.Views.Extensiones;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmEmpleado : Form
    {
        private Empleado emp;
        internal Empleado Emp { get => emp; set => emp = value; }
        public DataSet DSNomina { get; set; }
        private DaoEmpleadoImplements daoEmpleadoImplements;
        private DataRow drEmpleado;

        public FrmEmpleado()
        {
            daoEmpleadoImplements = new DaoEmpleadoImplements();
            InitializeComponent();
        }


        private void button2_Click(object sender, EventArgs e)
        {
            string name, lastname, address, id, phone;
            DateTime datatime;
            float salary;

            name = txtBNombres.Text;
            lastname = txtApellidos.Text;
            address = txtDireccion.Text;
            id = mtxtCedula.Text;
            phone = mtxtTelefono.Text;
            datatime = dtpFecha.Value;
            if (txtSalario.Text == "" || txtSalario.Text == null)
            {
                return;
            }
            else
            {
                salary = Convert.ToSingle(txtSalario.Text);
            }

            emp = new Empleado
            {
                Cedula = id,
                Nombres = name,
                Apellidos = lastname,
                Direccion = address,
                Telefono = phone,
                FechaContratacion = datatime,
                Salario = salary
            };
            daoEmpleadoImplements.Create(emp);

            Dispose();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
