﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmActualizar : Form
    {
        public List<Empleado> empleados;
        public int Row { get; set; }

        private DaoEmpleadoImplements daoEmpleado;
        public FrmActualizar()
        {
            empleados = new List<Empleado>();
            daoEmpleado = new DaoEmpleadoImplements();
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            validateForm();

            Empleado empleado = new Empleado();

            empleado.Id = Int32.Parse(txtId.Text);
            empleado.Nombres = txtBNombres.Text;
            empleado.Apellidos = txtApellidos.Text;
            empleado.Cedula = mtxtCedula.Text;
            empleado.Telefono = mtxtTelefono.Text;
            empleado.Salario = Single.Parse(txtSalario.Text);
            empleado.Direccion = txtDireccion.Text;
            empleado.FechaContratacion = dtpFecha.Value;

            daoEmpleado.Update(empleado);
            Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
